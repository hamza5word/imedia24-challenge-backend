import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

const BasicTable = ({pokemon}) => {
    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell>Id</TableCell>
                        <TableCell align="right">Name</TableCell>
                        <TableCell align="right">Type</TableCell>
                        <TableCell align="right">Height</TableCell>
                        <TableCell align="right">Order</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                        <TableRow key={pokemon.id} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                            <TableCell component="th" scope="row">{pokemon.id}</TableCell>
                            <TableCell align="right">{pokemon.name}</TableCell>
                            <TableCell align="right">{pokemon.types[0].type.name}</TableCell>
                            <TableCell align="right">{pokemon.height}</TableCell>
                            <TableCell align="right">{pokemon.order}</TableCell>
                        </TableRow>

                </TableBody>
            </Table>
        </TableContainer>
    );
}

export default BasicTable
