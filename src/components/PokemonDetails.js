import React, {useEffect, useState} from 'react'
import Modal from 'react-modal';
import * as Constants from "../constants/constants";
import BasicTable from "./DetailsTable";

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
    },
};

const PokemonDetails = ({ pokemonId }) => {

    let subtitle;
    const [modalIsOpen, setIsOpen] = useState(false)
    const [pokemonData, setPokemonData] = useState({})

    const getPokemonData = async () => {
        const res = await fetch(`${Constants.POKE_API_URL}/${pokemonId}`)
        const data = await res.json()
        setPokemonData(data)
    }

    useEffect(() => {
        getPokemonData().then(r => setPokemonData(r.data))
    }, [])

    const openModal = () => {
        setIsOpen(true);
    }

    const afterOpenModal = () => {
        // references are now sync'd and can be accessed.
        subtitle.style.color = '#f00';
    }

    const closeModal = () => {
        setIsOpen(false);
    }

    return (
        <div>
            <button onClick={openModal}>Details</button>
            <Modal
                isOpen={modalIsOpen}
                onAfterOpen={afterOpenModal}
                onRequestClose={closeModal}
                style={customStyles}
                contentLabel="Details Modal"
            >
                <div className="poke-details-container">
                    <div>
                        <img src={pokemonData.sprites.other.dream_world.front_default} alt={pokemonData.name} />
                        <h1 className="float-right">{pokemonData.name}</h1>
                    </div>
                    <div>
                        {<BasicTable pokemon={pokemonData}/>}
                    </div>
                </div>
            </Modal>
        </div>
    );
}

export default PokemonDetails
